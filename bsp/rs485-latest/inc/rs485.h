/*
 * rs485.h
 *
 * Change Logs:
 * Date           Author            Notes
 * 2020-06-08     qiyongzhong       first version
 */

#ifndef __DRV_RS485_H__
#define __DRV_RS485_H__

#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"
#include "semphr.h"

#include "usart.h"
//#define RS485_USING_TEST

#define RS485_BYTE_TMO_MIN      1
#define RS485_BYTE_TMO_MAX      15


struct rs485_inst 
{
 //   device_t serial;     //serial device handle
	UART_HandleTypeDef *phuart;

    SemaphoreHandle_t lock;        //mutex handle  SemaphoreHandle_t mutex = xSemaphoreCreateMutex();
    uint8_t status;      //connect status
    uint8_t level;       //control pin send mode level, 0--low, 1--high
    int16_t pin;         //control pin number used, -1--no using
    int32_t timeout;     //receive block timeout, ms   
    int32_t byte_tmo;    //receive byte interval timeout, ms
};

typedef struct rs485_inst rs485_inst_t;
extern rs485_inst_t rs485_hinst;

int rs485_create(rs485_inst_t * hinst);


/* 
 * @brief   destory rs485 instance created dynamically
 * @param   hinst       - instance handle
 * @retval  0 - success, other - error
 */
int rs485_destory(rs485_inst_t * hinst);

 
int rs485_config(rs485_inst_t * hinst );

/* 
 * @brief   set wait datas timeout for receiving 
 * @param   hinst       - instance handle
 * @param   tmo_ms      - receive wait timeout, 0--no wait, <0--wait forever, >0--wait timeout, default = 0
 * @retval  0 - success, other - error
 */
int rs485_set_recv_tmo(rs485_inst_t * hinst, int tmo_ms);
 
/* 
 * @brief   set byte interval timeout for receiving
 * @param   hinst       - instance handle
 * @param   tmo_ms      - byte interval timeout, default is calculated from baudrate
 * @retval  0 - success, other - error
 */
int rs485_set_byte_tmo(rs485_inst_t * hinst, int tmo_ms);

/* 
 * @brief   open rs485 connect
 * @param   hinst       - instance handle
 * @retval  0 - success, other - error
 */
int rs485_connect(rs485_inst_t * hinst);

/* 
 * @brief   close rs485 connect
 * @param   hinst       - instance handle
 * @retval  0 - success, other - error
 */
int rs485_disconn(rs485_inst_t * hinst);

/* 
 * @brief   receive datas from rs485
 * @param   hinst       - instance handle
 * @param   buf         - buffer addr
 * @param   size        - maximum length of received datas
 * @retval  >=0 - length of received datas, <0 - error
 */
int rs485_recv(rs485_inst_t * hinst, void *buf, int size);

/* 
 * @brief   send datas to rs485
 * @param   hinst       - instance handle
 * @param   buf         - buffer addr
 * @param   size        - length of send datas
 * @retval  >=0 - length of sent datas, <0 - error
 */
int rs485_send(rs485_inst_t * hinst, void *buf, int size);

/* 
 * @brief   break rs485 receive wait
 * @param   hinst       - instance handle
 * @retval  0 - success, other - error
 */
int rs485_break_recv(rs485_inst_t * hinst);

#endif

