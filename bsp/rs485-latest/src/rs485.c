/*
 * drv_rs485.c
 *
 * Change Logs:
 * Date           Author            Notes
 * 2020-06-08     qiyongzhong       first version
 */

 
#include <rs485.h>
#include "main.h"
//#include "qcloud_iot_export_log.h"
#include "usart.h"
#define LOG_TAG "drv.rs485"

//#include "elog.h"
#define log_e(info)   printf("err:%s",LOG_TAG);printf(info);
#define RS485_EVT_RX_IND    (1<<0)
#define RS485_EVT_RX_BREAK  (1<<1)

 
#define DE485_Pin				RS485_DE_Pin  
#define DE485_GPIO_Port   RS485_DE_GPIO_Port  
 
#define RS485_BAUDRATE (9600)

static int rs485_cal_byte_tmo(int baudrate)
{
    int tmo = (40 * 1000) / baudrate;
    if (tmo < RS485_BYTE_TMO_MIN)
    {
        tmo = RS485_BYTE_TMO_MIN;
    }
    else if (tmo > RS485_BYTE_TMO_MAX)
    {
        tmo = RS485_BYTE_TMO_MAX;
    }
    return (tmo);
}

static void rs485_mode_set(rs485_inst_t * hinst, int mode)//mode : 0--receive mode, 1--send mode
{
    if ( hinst->pin < 0)
    {
        return;
    }
    
    if (mode)
    {
		HAL_GPIO_WritePin(DE485_GPIO_Port, DE485_Pin , GPIO_PIN_SET);	 
    }
    else
    {
		HAL_GPIO_WritePin(DE485_GPIO_Port, DE485_Pin ,GPIO_PIN_RESET);         
    }
}

int rs485_create(rs485_inst_t * hinst)
{  
  // hinst->phuart = &RS485_HUART;

      hinst->lock = xSemaphoreCreateMutex();
 

     if ( hinst->lock == NULL)
    {
      //  log_e("rs485 create fail. no memory for rs485 create mutex.");
        return(NULL);
    }

   //  hinst->serial = dev;
    hinst->status = 0;
      hinst->timeout = 0;
    hinst->byte_tmo = rs485_cal_byte_tmo(115200);
    
   // printf("rs485 create success.");
	    rs485_mode_set(hinst, 0);//set to receive mode

    return (0);
}

int rs485_destory(rs485_inst_t * hinst)
{
    if (hinst == NULL)
    {
        log_e("rs485 destory fail. hinst is NULL.");
        return(-ERROR);
    }
    
    rs485_disconn(hinst);

//    if ( rs485_hinst.lock)
//    {
//        vSemaphoreDelete( rs485_hinst.lock);
//         rs485_hinst.lock = NULL;
//    }

 
    
     
    printf("rs485 destory success.");
    
    return(pdTRUE);
}

int rs485_config(rs485_inst_t * hinst )
{
 
    if (hinst == NULL)
    {
        log_e("rs485 config fail. hinst is NULL.");
        return(-ERROR);
    }

     
    return(pdTRUE);
}

int rs485_set_recv_tmo(rs485_inst_t * hinst, int tmo_ms)
{
    if (hinst == NULL)
    {
        log_e("rs485 set recv timeout fail. hinst is NULL.");
        return(-ERROR);
    }
    
     hinst->timeout = tmo_ms;
    
    printf("rs485 set recv timeout success. the value is %d.", tmo_ms);

    return(pdTRUE);
}

int rs485_set_byte_tmo(rs485_inst_t * hinst, int tmo_ms)
{
    if (hinst == NULL)
    {
        log_e("rs485 set byte timeout fail. hinst is NULL.");
        return(-ERROR);
    }
    
    if (tmo_ms < RS485_BYTE_TMO_MIN)
    {
        tmo_ms = RS485_BYTE_TMO_MIN;
    }
    else if (tmo_ms > RS485_BYTE_TMO_MAX)
    {
        tmo_ms = RS485_BYTE_TMO_MAX;
    }
    
     hinst->byte_tmo = tmo_ms;

    printf("rs485 set byte timeout success. the value is %d.", tmo_ms);

    return(pdTRUE);
}

int rs485_connect(rs485_inst_t * hinst)
{
       hinst->status = 1;

    printf("rs485 connect success.");

    return(pdTRUE);
}

/* 
 * @brief   close rs485 connect
 * @param   hinst       - instance handle
 * @retval  0 - success, other - error
 */
int rs485_disconn(rs485_inst_t * hinst)
{
   
    if ( hinst->status == 0)//is not connected
    {
        printf("rs485 is not connected.");
        return(-1);
    }

    xSemaphoreTake( hinst->lock, osWaitForever);
 
 
    
     hinst->status = 0;
    
    xSemaphoreGive( hinst->lock);
    
    printf("rs485 disconnect success.");
    
    return(pdTRUE);
}

/* 
 * @brief   receive datas from rs485
 * @param   hinst       - instance handle
 * @param   buf         - buffer addr
 * @param   size        - maximum length of received datas
 * @retval  >=0 - length of received datas, <0 - error
 */
int rs485_recv(rs485_inst_t * hinst, void *buf, int size)
{
    int recv_len = 0;
    uint32_t recved = 0;
    
    if (hinst == NULL || buf == NULL || size == 0)
    {
        log_e("rs485 receive fail. param error.");
        return(-ERROR);
    }
    
    if (xSemaphoreTake( hinst->lock, osWaitForever) != pdTRUE)
    {
        log_e("rs485 receive fail. it is destoried.");
        return(-ERROR);
    }
    
    if ( hinst->status == 0)
    {
        xSemaphoreGive( hinst->lock);
        log_e("rs485 receive fail. it is not connected.");
        return(-ERROR);
    }

 
    
    while(size)
    {
		int len = 0;
    //   int len = device_read( hinst->serial, 0, buf + recv_len, size);
//			        recv_len = read_resp(tmp, buf);

        if (len)
        {
            recv_len += len;
            size -= len;
            continue;
        }
      
    }
    
    xSemaphoreGive( hinst->lock);
    
    return(recv_len);
}

/* 
 * @brief   send datas to rs485
 * @param   hinst       - instance handle
 * @param   buf         - buffer addr
 * @param   size        - maximum length of received datas
 * @retval  >=0 - length of sent datas, <0 - error
 */
int rs485_send(rs485_inst_t * hinst, void *buf, int size)
{
    int send_len = 0;
    
    if (hinst == NULL || buf == NULL || size == 0)
    {
        log_e("rs485 send fail. param is error.");
        return(-ERROR);
    }
    
    if (xSemaphoreTake( hinst->lock, osWaitForever) != pdTRUE)
    {
        log_e("rs485 send fail. it is destoried.");
        return(-ERROR);
    }

    if ( hinst->status == 0)
    {
        xSemaphoreGive( hinst->lock);
        log_e("rs485 send fail. it is not connected.");
        return(-ERROR);
    }

    rs485_mode_set(hinst, 1);//set to send mode

 //   send_len = device_write( hinst->serial, 0, buf, size);
	 HAL_UART_Transmit(hinst->phuart, buf, size, 0xFFFF);
	 

    rs485_mode_set(hinst, 0);//set to receive mode
    
    xSemaphoreGive( hinst->lock);

    return(send_len);
}

 
