/*
 * rs485_test.c
 *
 * Change Logs:
 * Date           Author            Notes
 * 2020-06-08     qiyongzhong       first version
 */

 
#include <rs485.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "rtosal.h"
 
//#if (USE_TRACE_ATCORE == 1U)
//#if (USE_PRINTF  == 0U)
//#include "trace_interface.h"
//#define TRACE_INFO(format, args...) TRACE_PRINT(DBG_CHAN_ATCMD, DBL_LVL_P0, "ATCore:" format "\n\r", ## args)
//#define TRACE_DBG(format, args...)  TRACE_PRINT(DBG_CHAN_ATCMD, DBL_LVL_P1, "ATCore:" format "\n\r", ## args)
//#define TRACE_ERR(format, args...)  TRACE_PRINT(DBG_CHAN_ATCMD, DBL_LVL_ERR, "ATCore ERROR:" format "\n\r", ## args)
//#else
//#define TRACE_INFO(format, args...)  (void) printf("ATCore:" format "\n\r", ## args);
//#define TRACE_DBG(...)   __NOP(); /* Nothing to do */
//#define TRACE_ERR(format, args...)   (void) printf("ATCore ERROR:" format "\n\r", ## args);
//#endif /* USE_PRINTF */
//#else
//#define TRACE_INFO(...)   __NOP(); /* Nothing to do */
//#define TRACE_DBG(...)   __NOP(); /* Nothing to do */
//#define TRACE_ERR(...)   __NOP(); /* Nothing to do */
//#endif /* USE_TRACE_ATCORE */


//#define	 RS485_HUART	 huart5
#define RS485_TEST_BUF_SIZE     256             //default test buffer size
#define USER_DEFINED_IPC_DEVICE_RS485  (IPC_DEVICE_0)
#define SIG_IPC_MSG                      (1U) /* signals definition for IPC message queue */
#define MSG_IPC_RECEIVED_SIZE (uint32_t) ((uint16_t) 128U)
#define RCV_SIZE_MAX 		128

#define RS485_THREAD_STACK_SIZE            (384U)

//static rs485_inst_t * test_hinst = NULL;
static char test_buf[RS485_TEST_BUF_SIZE] ={"hello rs485\r\n"};

 rs485_inst_t rs485_hinst;
#define RS485_HUART       huart2
static __IO uint8_t    rs485_MsgReceived = 0; /* array of rx msg counters (1 per ATCore handler) */
static osThreadId rs485TaskId = NULL;


static void IRQ_DISABLE(void)
{
  __disable_irq();
}

static void IRQ_ENABLE(void)
{
  __enable_irq();
}
 
  uint8_t rcvChar[RCV_SIZE_MAX];
  int16_t rcv_size;

static void RS485TaskBody(void *argument)
{
  //UNUSED(argument);

    int ret;
   rtosalStatus status;
  uint32_t msg = 0;

 
  //log_d("<start ATCore TASK>");

  /* Infinite loop */
  for (;;)
  {
    /* waiting IPC message received event (message) */
		rcv_size = RCV_SIZE_MAX;
 
	  if (ret == 0)
	  {
		  /* traceIF_hexPrint(DBG_CHAN_PPPOSIF, DBL_LVL_P0, rcvChar, rcv_size) */
		  /* Pass received data to PPPoS to be decoded through lwIP TCPIP thread */
//		  (void)pppos_input_tcpip(p_ppp_pcb, rcvChar, rcv_size);
		  __nop();
	  }
	  vTaskDelay(20);
     
  
  }
}
 

 
 

// uint8_t ATCustom_TYPE1SC_checkEndOfMsgCallback(uint8_t rxChar)
//{
//  uint8_t last_char = 0U;

//  /*---------------------------------------------------------------------------------------*/
//  if (TYPE1SC_ctxt.state_SyntaxAutomaton == WAITING_FOR_INIT_CR)
//  {
//    /* waiting for first valid <CR>, char received before are considered as trash */
//    if ((AT_CHAR_t)('\r') == rxChar)
//    {
//      /* current     : xxxxx<CR>
//      *  command format : <CR><LF>xxxxxxxx<CR><LF>
//      *  waiting for : <LF>
//      */
//      TYPE1SC_ctxt.state_SyntaxAutomaton = WAITING_FOR_LF;
//    }
//  }
//  /*---------------------------------------------------------------------------------------*/
//  else if (TYPE1SC_ctxt.state_SyntaxAutomaton == WAITING_FOR_CR)
//  {
//    if ((AT_CHAR_t)('\r') == rxChar)
//    {
//      /* current     : xxxxx<CR>
//      *  command format : <CR><LF>xxxxxxxx<CR><LF>
//      *  waiting for : <LF>
//      */
//      TYPE1SC_ctxt.state_SyntaxAutomaton = WAITING_FOR_LF;
//    }
//  }
//  /*---------------------------------------------------------------------------------------*/
//  else if (TYPE1SC_ctxt.state_SyntaxAutomaton == WAITING_FOR_LF)
//  {
//    /* waiting for <LF> */
//    if ((AT_CHAR_t)('\n') == rxChar)
//    {
//      /*  current        : <CR><LF>
//      *   command format : <CR><LF>xxxxxxxx<CR><LF>
//      *   waiting for    : x or <CR>
//      */
//      TYPE1SC_ctxt.state_SyntaxAutomaton = WAITING_FOR_FIRST_CHAR;
//      last_char = 1U;
//    }
//  }
//  /*---------------------------------------------------------------------------------------*/
//  else if (TYPE1SC_ctxt.state_SyntaxAutomaton == WAITING_FOR_FIRST_CHAR)
//  {
//    /* NOTE about Socket mode:
//    * No need to use/manage socket_RxData_state as it is done for other modems.
//    * Indeed as data are received in HEX format, no risk to detect <CR> or <LF>
//    * in the received data.
//    */

//    /* waiting for <CR> or x */
//    if ((AT_CHAR_t)('\r') == rxChar)
//    {
//      /*   current        : <CR>
//      *   command format : <CR><LF>xxxxxxxx<CR><LF>
//      *   waiting for    : <LF>
//      */
//      TYPE1SC_ctxt.state_SyntaxAutomaton = WAITING_FOR_LF;
//    }
//    else {/* nothing to do */ }
//  }
//  /*---------------------------------------------------------------------------------------*/
//  else
//  {
//    /* should not happen */
//    __NOP();
//  }

//  /* ###########################  START CUSTOMIZATION PART  ######################### */
//  /* if modem does not use standard syntax or has some specificities, replace previous
//  *  function by a custom function
//  *  for example, apply special treatment if (last_char = 0U)
//  */

//  /* ###########################  END CUSTOMIZATION PART  ########################### */

//  return (last_char);
//}
  void rs485_test(void )
{
	int size;
	rs485_hinst.phuart = &RS485_HUART;
	rs485_create(&rs485_hinst);
	      /* queues creation */

      /* start driver thread */
      rs485TaskId = rtosalThreadNew((const rtosal_char_t *)"rs485Task",
                                     (os_pthread) RS485TaskBody,
                                     osPriorityNormal,
                                     (uint32_t)RS485_THREAD_STACK_SIZE,
                                     NULL);
     if (rs485_connect(&rs485_hinst) == pdTRUE)
        {
            printf("rs485 instance connect success.\n");
        }
        else
        {
            printf("rs485 instance connect fail.\n");
        }
  
 
    
 
 
// 
//    
//        len = rs485_recv(test_hinst, test_buf, size);
//        if (len == 0)
//        {
//            printf("rs485 receive timeout.\n");
//            return;
//        }
// 
    
 
    //    size = rs485_send(&rs485_hinst, test_buf, strlen(test_buf));
    

 }
 
