#ifndef ZR_SERIAL_H
#define ZR_SERIAL_H

#include "main.h"

enum {
    AT_USART_RX,
    AT_TASK_QUIT,
    AT_SENT_DONE
}; 
typedef struct {
    uint32_t ori;
    uint32_t end;
    uint32_t msg_type;
} recv_buff;

void at_transmit(uint8_t * cmd, int32_t len,int flag);
int32_t at_usart_init(void);
void at_usart_deinit(void);
int read_resp(uint8_t *buf, recv_buff* recv_buf);
void write_at_task_msg(uint32_t type);

#endif /* _AT_HAL_H */