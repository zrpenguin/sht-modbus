#include "main.h"
#include <string.h>
//#include "bsp_serial.h"
//#include "bsp_rs485.h"
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"
#include "semphr.h"
#include "queue.h"
#include "usart.h"
#include "zr_serial.h"
 #include "rtosal.h" 
 #include "rs485.h"
#define MAX_USART_BUF_LEN 		1024
#define LINE_END 				"\r\n"
#define MSGQUEUE_OBJECTS 					16                     // number of Message Queue Objects
#define SERIAL_THREAD_STACK_SIZE            (512U)

#define my_usart  huart2

uint8_t buff_full = 0;
static uint32_t g_disscard_cnt = 0;

static uint32_t wi       = 0;
static uint32_t pre_ri   = 0; /* only save cur msg start */
static uint32_t ri       = 0;
static uint8_t uart_recv_buf[MAX_USART_BUF_LEN];
osMessageQueueId_t mid_MsgQueue;                // message queue id
static osThreadId serialTaskId = NULL;
 
void myusart_irq_handler(void)
{
    recv_buff recv_buf;

    if (__HAL_UART_GET_FLAG(&my_usart, UART_FLAG_RXNE) != RESET) {
        uart_recv_buf[wi++] = (uint8_t)(my_usart.Instance->DR & 0x00FF);
        if (wi == ri) {
            buff_full = 1;
        }
        if (wi >=   MAX_USART_BUF_LEN) {
            wi = 0;
        }
    } else if (__HAL_UART_GET_FLAG(&my_usart, UART_FLAG_IDLE) != RESET) {
        __HAL_UART_CLEAR_IDLEFLAG(&my_usart);
        /*
        Ring Buffer ri------------------------>wi

         __________________________________________________
         |      msg0           |  msg1        |   msg2    |
         ri(pre_ri0)        pre_ri1         pre_ri2     wi(pre_ri3)
         __________________________________________________

         read_resp ---->ri= pre_ri1----------->---------->ri=wi=pre_ri3(end)
        */
        recv_buf.ori = pre_ri;
        recv_buf.end = wi;

        pre_ri = recv_buf.end;
        recv_buf.msg_type =  AT_USART_RX;

        if (osMessageQueuePut(mid_MsgQueue, &recv_buf, NULL, 0x100) != HAL_OK) {
            g_disscard_cnt++;
        }
    }
}

int32_t my_usart_init(void)
{
	mid_MsgQueue = osMessageQueueNew(MSGQUEUE_OBJECTS, sizeof(recv_buff), NULL);
    __HAL_UART_CLEAR_FLAG(&my_usart, UART_FLAG_TC);
     __HAL_UART_ENABLE_IT(&my_usart, UART_IT_IDLE);
    __HAL_UART_ENABLE_IT(&my_usart, UART_IT_RXNE);
    return HAL_OK;
}

void my_usart_deinit(void)
{
    UART_HandleTypeDef *husart = &my_usart;
    __HAL_UART_DISABLE(husart);
    __HAL_UART_DISABLE_IT(husart, UART_IT_IDLE);
    __HAL_UART_DISABLE_IT(husart, UART_IT_RXNE);
}

void usart_transmit(uint8_t *cmd, int32_t len, int flag)
{
     
     (void)HAL_UART_Transmit(&my_usart, (uint8_t *)cmd, len, 0xffff);
    if (flag == 1) {
        (void)HAL_UART_Transmit(&my_usart, (uint8_t *)LINE_END, strlen(LINE_END), 0xffff);
    }
}

int read_resp(uint8_t *buf, recv_buff *recv_buf)
{
    uint32_t len = 0;
    uint32_t tmp_len;

 
    if (NULL == buf) {
        return -1;
    }

    if (buff_full == 1) {
       // AT_LOG("buf maybe full,buff_full is %d", buff_full);
    }

    if (recv_buf->end == recv_buf->ori) {
        len = 0;
        goto END;
    }

    if (recv_buf->end > recv_buf->ori) {
        len = recv_buf->end - recv_buf->ori;
        (void)memcpy(buf,  &uart_recv_buf[recv_buf->ori], len);
    } else {
        tmp_len = MAX_USART_BUF_LEN - recv_buf->ori;
        (void)memcpy(buf,  &uart_recv_buf[recv_buf->ori], tmp_len);
        (void)memcpy(buf + tmp_len,  uart_recv_buf, recv_buf->end);
        len = recv_buf->end + tmp_len;
    }

    ri = recv_buf->end;

END:
    return len;
}

void write_at_task_msg(uint32_t type)
{
    recv_buff recv_buf;
    int ret;

    (void)memset(&recv_buf,   0, sizeof(recv_buf));
    recv_buf.msg_type = type;

  //  ret = LOS_QueueWriteCopy(mid_MsgQueue, &recv_buf, sizeof(recv_buff), 0);
  
}
uint32_t usart_write(  int8_t *buf, int32_t len)
{
    const char *suffix_array[1];
 
    return len;
}
//static void at_handle_resp(int8_t *resp_buf, uint32_t resp_len)
//{
//    at_listener *listener = NULL;

//    listener = at.head;
//    if (NULL == listener)
//        return;

//    if (at_handle_callback_cmd_resp(listener, resp_buf, resp_len) == AT_OK)
//    {
//        return;
//    }

//    if(listener->cmd_info.suffix == NULL)
//    {

//        //store_resp_buf((int8_t *)listener->resp, (int8_t*)p1, p2 - p1);
//        (void)LOS_SemPost(at.resp_sem);
//        listener = NULL;
//        return;
//    }

//    for (uint32_t i = 0;  i < listener->cmd_info.suffix_num; i++)
//    {
//        char *suffix;

//        if (listener->cmd_info.suffix[i] == NULL)
//        {
//            continue;
//        }

//        suffix = strstr((char *)resp_buf, (const char *)listener->cmd_info.suffix[i]);
//        if (suffix != NULL)
//        {
//            if ((NULL != listener->cmd_info.resp_buf) && (NULL != listener->cmd_info.resp_len) && (resp_len > 0))
//            {
//                store_resp_buf((int8_t *)listener->cmd_info.resp_buf, resp_buf, resp_len, listener->cmd_info.resp_len);//suffix + strlen((char *)listener->suffix) - p1
//            }
//            listener->cmd_info.match_idx = i;
//            (void)LOS_SemPost(at.resp_sem);
//            break;
//        }

//    }
//}
uint8_t userdata_recv[MAX_USART_BUF_LEN]={0};
 void uart_recv_task(void)
{
    uint32_t recv_len = 0;
    uint8_t *tmp =  userdata_recv;  //[MAX_USART_BUF_LEN] = {0};
    int ret = 0;
    recv_buff recv_buf;
    uint32_t rlen = sizeof(recv_buff);
		printf("running in recv task\r\n");
    while(1)
    {
         //AT_LOG("www wait time %ld", wait_time);
        ret = osMessageQueueGet(mid_MsgQueue, &recv_buf, NULL, osWaitForever);
				if (ret == osOK) 
				{
					__nop(); // process data
					printf("recv msg %d ori is %d end is %d \r\n",recv_buf.msg_type,recv_buf.ori,recv_buf.end);
				}
				else
				{
						continue;
				}
//		if(ret != HAL_OK)
//        {
//            continue;
//        }
//        if (recv_buf.msg_type  != AT_USART_RX  )
//        {
//             continue;
//        }

        memset(userdata_recv, 0,MAX_USART_BUF_LEN);
        recv_len = read_resp(userdata_recv, &recv_buf);

        if (recv_len <= 0)
        {
            //AT_LOG("err, recv_len = %ld", recv_len);
            continue;
        }
				printf("rec data is:");
				for(int i=0;i<recv_len;i++)
					printf(" 0x%x ",userdata_recv[i]);

				printf("\r\n");

      //  at_handle_resp((int8_t *)tmp, recv_len);
 
    }
}
static rs485_inst_t rs485_test;
uint32_t create_at_recv_task(void)
{
    uint32_t uwRet = HAL_OK;
	my_usart_init();
		rs485_test.phuart = &huart2;
	rs485_create(&rs485_test);
	  rs485_connect(&rs485_test);
	
	serialTaskId = rtosalThreadNew((const rtosal_char_t *)"recv_task",(os_pthread)uart_recv_task,osPriorityNormal,SERIAL_THREAD_STACK_SIZE,NULL);  
 
    return 0;

}