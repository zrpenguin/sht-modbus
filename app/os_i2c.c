#include "main.h"
#include "cmsis_os.h"
#include "osprintf.h"
#include "os_i2c.h"
#include "i2c.h"

osMutexId I2C_MutexHandle ;
#define    I2C_EXPBD_Handle  hi2c1
#define 	I2C_EXPBD                            I2C1
#define  heval_I2c		hi2c1
#define IIC2_SCLH  	 	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8,GPIO_PIN_SET)	 
#define IIC2_SCLL	 	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8,GPIO_PIN_RESET)	 
#define IIC2_SDAH  	 	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_9,GPIO_PIN_SET)	 
#define IIC2_SDAL	 	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_9,GPIO_PIN_RESET)	 

static void I2C_EXPBD_Error( uint8_t Addr );
const uint32_t I2C_EXPBD_Timeout = 0x1000;    /*<! Value of Timeout when I2C communication fails */
static unsigned short RETRY_IN_MLSEC  = 55;
 void mdelay(unsigned long nTime)
{
//	TimingDelay = nTime;
//	while(TimingDelay != 0);
	HAL_Delay(nTime);
}
void Set_I2C_Retry(unsigned short ml_sec)
{
  RETRY_IN_MLSEC = ml_sec;
}

unsigned short Get_I2C_Retry()
{
  return RETRY_IN_MLSEC;
}

void os_iic(void)
{
	osMutexDef(osI2C_Mutex);
    I2C_MutexHandle = osMutexNew(osMutex(osI2C_Mutex));
    if (I2C_MutexHandle == NULL)
    {
 //     result = CELLULAR_FALSE;
     __nop();
    }
   
  
  osMutexRelease(I2C_MutexHandle);
}
/**
 * @brief  Write data to the register of the device through BUS
 * @param  Addr Device address on BUS
 * @param  Reg The target register address to be written
 * @param  pBuffer The data to be written
 * @param  Size Number of bytes to be written
 * @retval 0 in case of success
 * @retval 1 in case of failure
 */
static uint8_t I2C_EXPBD_WriteData( uint8_t Addr, uint8_t Reg,  uint16_t Size, uint8_t* pBuffer)
{

  HAL_StatusTypeDef status = HAL_OK;

 //HAL_I2C_Mem_Write( &I2C_EXPBD_Handle, Addr, ( uint16_t )Reg, I2C_MEMADD_SIZE_8BIT, pBuffer, Size,                              I2C_EXPBD_Timeout );	
  status = HAL_I2C_Mem_Write( &I2C_EXPBD_Handle, Addr, ( uint16_t )Reg, I2C_MEMADD_SIZE_8BIT, pBuffer, Size,
                              I2C_EXPBD_Timeout );
  /* Check the communication status */
  if( status != HAL_OK )
  {

    /* Execute user timeout callback */
    I2C_EXPBD_Error( Addr );
    return 1;
  }
  else
  {
    return 0;
  }
}



/**
 * @brief  Read a register of the device through BUS
 * @param  Addr Device address on BUS
 * @param  Reg The target register address to read
 * @param  pBuffer The data to be read
 * @param  Size Number of bytes to be read
 * @retval 0 in case of success
 * @retval 1 in case of failure
 */
static uint8_t I2C_EXPBD_ReadData( uint8_t Addr, uint16_t Reg,  uint16_t Size,uint8_t* pBuffer )
{

  HAL_StatusTypeDef status = HAL_OK;

 status = HAL_I2C_Mem_Read( &I2C_EXPBD_Handle, Addr, ( uint16_t )Reg, I2C_MEMADD_SIZE_8BIT, pBuffer, Size,  I2C_EXPBD_Timeout );


  /* Check the communication status */
  if( status != HAL_OK )
  {

    /* Execute user timeout callback */
    I2C_EXPBD_Error( Addr );
    return 1;
  }
  else
  {
    return 0;
  }
}

 int Sensors_I2C_WriteRegister(unsigned char slave_addr, unsigned char reg_addr,unsigned short len, unsigned char *data_ptr)
{
  char retries=0;
  int ret = 0;
  unsigned short retry_in_mlsec = Get_I2C_Retry();
                              
tryWriteAgain:  
  ret = 0;
  ret = I2C_EXPBD_WriteData( slave_addr, reg_addr, len, data_ptr); 

  if(ret && retry_in_mlsec)
  {
    if( retries++ > 4 )
        return ret;
    
    mdelay(retry_in_mlsec);
    goto tryWriteAgain;
  }
  return ret;  
}

int Sensors_I2C_ReadRegister(unsigned char slave_addr,
                                       unsigned char reg_addr,
                                       unsigned short len, 
                                       unsigned char *data_ptr)
{
  char retries=0;
  int ret = 0;
  unsigned short retry_in_mlsec = Get_I2C_Retry();
  
tryReadAgain:  
  ret = 0;
  ret = I2C_EXPBD_ReadData( slave_addr, reg_addr, len, data_ptr);

  if(ret && retry_in_mlsec)
  {
    if( retries++ > 4 )
        return ret;
    
    mdelay(retry_in_mlsec);
    goto tryReadAgain;
  } 
  return ret;
}


int os_I2C_WriteReg(unsigned char slave_addr, unsigned char reg_addr,unsigned short len, unsigned char *data_ptr)
{
	int result;
	osMutexWait(I2C_MutexHandle, osWaitForever);
	result=	Sensors_I2C_WriteRegister( slave_addr,  reg_addr, len,data_ptr);

  osMutexRelease(I2C_MutexHandle);
	return result;
}
int os_I2C_ReadReg(unsigned char slave_addr,unsigned char reg_addr,unsigned short len,unsigned char *data_ptr)
{
	int result;
	osMutexWait(I2C_MutexHandle, osWaitForever);
	result	=	Sensors_I2C_ReadRegister( slave_addr,  reg_addr, len,data_ptr);
	osMutexRelease(I2C_MutexHandle);
	return result;
}
  void I2Cx_Reset(I2C_HandleTypeDef* i2cHandle)
{
	
  GPIO_InitTypeDef GPIO_InitStruct;
	uint8_t i;
 
  /*Configure GPIO pin :   */
 if( i2cHandle->Instance==I2C2)
 {
  
    GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
     HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	 HAL_Delay(100);
	 IIC2_SDAH;
	 IIC2_SCLH;
	 for(i = 0; i < 9; i++)
	 {
		  IIC2_SCLL;
		 HAL_Delay(1);
		IIC2_SCLH;
	 }
	 IIC2_SCLH;
	 
 }
}
/**
 * @brief  Configures I2C interface.
 * @param  None
 * @retval 0 in case of success
 * @retval 1 in case of failure
 */
static uint8_t I2C_EXPBD_Init( void )
{
  if(HAL_I2C_GetState( &I2C_EXPBD_Handle) == HAL_I2C_STATE_RESET )
  {

    /* I2C_EXPBD peripheral configuration */

//    I2C_EXPBD_Handle.Init.ClockSpeed = 400000;
//    I2C_EXPBD_Handle.Init.DutyCycle = I2C_DUTYCYCLE_2;
// 

// 
//    I2C_EXPBD_Handle.Init.OwnAddress1    = 0x33;
//    I2C_EXPBD_Handle.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
//    I2C_EXPBD_Handle.Instance            = I2C_EXPBD;

    /* Init the I2C */
    MX_I2C1_Init();
    
  }

  if( HAL_I2C_GetState( &I2C_EXPBD_Handle) == HAL_I2C_STATE_READY )
  {
    return 0;
  }
  else
  {
    return 1;
  }
}

/**
 * @brief  Manages error callback by re-initializing I2C
 * @param  Addr I2C Address
 * @retval None
 */
static void I2C_EXPBD_Error( uint8_t Addr )
{

  /* De-initialize the I2C comunication bus */
	    I2Cx_Reset( &I2C_EXPBD_Handle);
 
	
  HAL_I2C_DeInit( &I2C_EXPBD_Handle );

  /* Re-Initiaize the I2C comunication bus */
  I2C_EXPBD_Init();
}