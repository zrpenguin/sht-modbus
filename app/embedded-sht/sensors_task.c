#include "main.h"
#include <stdio.h>
#include "stm32l4xx_hal.h"
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"
#include "osprintf.h"
#include "sensirion_common.h"
#include "sht3x.h"
extern int8_t i2c_reg_write(uint8_t i2c_addr, uint8_t reg_addr, uint8_t *reg_data, uint16_t length);
extern int8_t i2c_reg_read(uint8_t i2c_addr, uint8_t reg_addr, uint8_t *reg_data, uint16_t length);

 
typedef struct    {
	int32_t temperature;
	int32_t humidity;
 
 }Sensors_output_t;
Sensors_output_t sensors_upload;
 

 
void StartTaskSensorsGet( void *pvParameters )
{
    int8_t rslt;  
	int8_t accuracy;
    TickType_t xLastWakeTime;
	const TickType_t xFrequency = 2000;
     // Initialise the xLastWakeTime variable with the current time.
     xLastWakeTime = xTaskGetTickCount();
 
	 while (sht3x_probe() != STATUS_OK) 
	{
        printf("SHT sensor probing failed\n"); 
    }
	sht3x_measure();
	while(1)
	{
		
        /* Measure temperature and relative humidity and store into variables
         * temperature, humidity (each output multiplied by 1000).
         */
		
        int8_t ret = sht3x_measure_blocking_read(&sensors_upload.temperature, &sensors_upload.humidity);
        if (ret == STATUS_OK) 
				{
             osprintf("measured temperature: %0.2f degreeCelsius, "
                      "measured humidity: %0.2f percentRH\n",
                      sensors_upload.temperature / 1000.0f,
                      sensors_upload.humidity / 1000.0f); 
        } else 
				{
            printf("error reading measurement\n"); 
        }
 		
	  
//		LED_Toggle(LED4); 
		    HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);

		vTaskDelayUntil( &xLastWakeTime, xFrequency );
	}
}
 